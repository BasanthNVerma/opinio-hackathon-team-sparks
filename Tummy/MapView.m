//
//  MapView.m
//  Tummy
//
//  Created by Priyanka on 5/15/16.
//  Copyright © 2016 Priyanka. All rights reserved.
//

#import "MapView.h"
#import <GLKit/GLKit.h>
#import <CoreLocation/CoreLocation.h>
#import "LatLong.h"

@interface MapView ()

@end

@implementation MapView
-(instancetype)initWithFrame:(CGRect)frame srcLocation:(CLLocation *)srcLoc andDestinationLoc:(CLLocation *)destLoc
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _srcLoc = srcLoc;
        _destLoc = destLoc;
        _latLongsArray = [[NSMutableArray alloc]init];
        [self allocateViews];
    }
    
    return self;
}

//Setup all views needed for map
-(void)allocateViews
{
    [self createMap];
}

//Map view creation
-(void)createMap
{
    _mapView = [[GMSMapView alloc]init];
    _mapView.mapType = kGMSTypeNormal;
    
    _trackPath = [GMSMutablePath path];
    GMSCameraPosition *mapCamera = [GMSCameraPosition cameraWithLatitude:_destLoc.coordinate.latitude longitude:_destLoc.coordinate.longitude zoom:15];
    _mapView.camera = mapCamera;
    [self addSubview:_mapView];
    
    
    if (_trackPath == nil)
        _trackPath = [GMSMutablePath path];
    
    {
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(_destLoc.coordinate.latitude,_destLoc.coordinate.longitude);
        marker.icon = [UIImage imageNamed:@"me.png"];
        marker.map = _mapView;
    }
    
    [_trackPath addLatitude:_destLoc.coordinate.latitude longitude:_destLoc.coordinate.longitude];
    [self updateCameraWithLocation:_destLoc enableZoom:YES];
    
    LatLong *latLong = [[LatLong alloc]init];
    latLong.latitude = _destLoc.coordinate.latitude;
    latLong.longitude = _destLoc.coordinate.longitude;
    
    [_latLongsArray addObject:latLong];

    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(_srcLoc.coordinate.latitude,_srcLoc.coordinate.longitude);
    marker.icon = [UIImage imageNamed:@"groundPilot.png"];
    marker.map = _mapView;
    
    CLLocationCoordinate2D twodCoordinate = CLLocationCoordinate2DMake((_destLoc.coordinate.latitude+_srcLoc.coordinate.latitude)/2, (_destLoc.coordinate.longitude+_srcLoc.coordinate.longitude)/2);
    
    GMSCameraPosition *cameraPosition = [[GMSCameraPosition alloc]initWithTarget:twodCoordinate zoom:12.5 bearing:[self angleBteweenCoordinate:_destLoc andPoint2:_srcLoc] viewingAngle:50];
    [_mapView setCamera:cameraPosition];
    
    _trackLine = [GMSPolyline polylineWithPath:_trackPath];
    _trackLine.strokeColor = [UIColor blueColor];
    _trackLine.strokeWidth = 4.0f;
    _trackLine.map = _mapView;
    
    
}

-(double)angleBteweenCoordinate:(CLLocation*)point1 andPoint2:(CLLocation *)point2
{
    double long1 = point1.coordinate.longitude;
    double long2 = point2.coordinate.longitude;
    double lat1 = point1.coordinate.longitude;
    double lat2 = point2.coordinate.longitude;
    
    double dLon = (long2 - long1);
    
    double y = sin(dLon) * cos(lat2);
    double x = cos(lat1) * sin(lat2) - sin(lat1)
    * cos(lat2) * cos(dLon);
    
    double brng = atan2(y, x);
    brng = GLKMathRadiansToDegrees(brng);
    brng = 360 - brng;
    
    return brng;
}


//Add Nodes for Creating route
-(void)addPoint:(CLLocation *)location withSource:(BOOL)hasSource
{
    [_trackPath addLatitude:location.coordinate.latitude longitude:location.coordinate.longitude];
    _trackLine.path = _trackPath;
    
    LatLong *latLong = [[LatLong alloc]init];
    latLong.latitude = location.coordinate.latitude;
    latLong.longitude = location.coordinate.longitude;
    
    [_latLongsArray addObject:latLong];
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude);
    //   marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.icon = [UIImage imageNamed:@"me.png"];
    marker.map = _mapView;
    
    if ([location distanceFromLocation:_srcLoc] < 500) {
        
        [_trackPath addLatitude:_srcLoc.coordinate.latitude longitude:_srcLoc.coordinate.longitude];
        _trackLine.path = _trackPath;
        
        
        LatLong *latLong = [[LatLong alloc]init];
        latLong.latitude = _srcLoc.coordinate.latitude;
        latLong.longitude = _srcLoc.coordinate.longitude;
        
        [_latLongsArray addObject:latLong];
    }
    
    if (hasSource) {
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(location.coordinate.latitude+0.1, location.coordinate.longitude+0.1);
        marker.icon = [UIImage imageNamed:@"pin.png"];
        marker.map = _mapView;
    }
    
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    _mapView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}


//Update camera for for new coordinate
-(void)updateCameraWithLocation:(CLLocation *)location enableZoom:(BOOL)isZoomEnabled

{
    CLLocationCoordinate2D latestLocation = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
    GMSCameraUpdate *updatedCamera;
    
    if (isZoomEnabled)
        updatedCamera = [GMSCameraUpdate setTarget:latestLocation zoom:15];
    else
        updatedCamera = [GMSCameraUpdate setTarget:latestLocation];
    
    [_mapView animateWithCameraUpdate:updatedCamera];
    
}



@end