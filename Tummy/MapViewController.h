//
//  MapViewController.h
//  Tummy
//
//  Created by Priyanka on 5/14/16.
//  Copyright © 2016 Priyanka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LatLong.h"

@interface MapViewController : UIViewController
@property (nonatomic) NSUInteger orderId;

-(LatLong *) getLocationFromAddressString: (NSString*) addressStr;
-(void)updateServerWithOrderID:(NSString *)orderID source:(NSString *)source destination:(NSString *)dest andNodeArray:(NSArray *)nodeArray
;
@end
