//
//  FetchDistanceAndTimeOperation.m
//  NetworkOp
//
//  Created by Sunil Rao on 14/05/16.
//  Copyright © 2016 Sunil Rao. All rights reserved.
//

#import "FetchDistanceAndTimeOperation.h"

#define SOURCE_LAT_KEY              @"startPointLat"
#define SOURCE_LONG_KEY             @"startPointLong"
#define DESTINATION_LAT_KEY         @"endPointLat"
#define DESTINATION_LONG_KEY        @"endPointLong"
#define ORDER_ID                    @"orderId"
#define NODE_LAT                    @"nodeLatitude"
#define NODE_LONG                   @"nodeLongitude"
#define NODE_DATA                   @"latLongDataList"
#define IS_CUSTOMER_ROUTE           @"isCustomerGiven"

#define CONTENT_TYPE                @"application/json"
#define URL_STRING                  @"http://172.31.99.135:8080/saveDist"

@interface FetchDistanceAndTimeOperation()<NSURLConnectionDelegate>

@property (nonatomic,strong) NSString *source, *destination;
@property (nonatomic,strong) NSMutableData *completionData;
@property (nonatomic, strong) NSURLConnection *connection;
@property (nonatomic, strong) NSMutableData *data;
@property (nonatomic, assign) NSUInteger numberOfBytesReceivedBeforeNotifying;
@property (nonatomic,strong) NSArray *nodeArray;
@property (nonatomic,strong) NSString *orderID;

// Typedefs
typedef void (^CompletionBlockNoParams)();
typedef void (^CompletionBlockParam)(id param);
typedef void (^ErrorBlockParam)(id param);

@property (nonatomic, copy) CompletionBlockParam completeBlock;
@property (nonatomic, copy) ErrorBlockParam errorBlock;
@property (nonatomic, strong) NSThread *sourceThread;


@end

@implementation FetchDistanceAndTimeOperation
{
    long long _notifySize;
}
- (id)initWithSource:(NSString *)source Destination:(NSString *)dest OrderID:(NSString *)orderID andNodeArray:(NSArray *)nodeArray
{
    self = [super init];
    
    if (self)
    {
        self.source = source;
        self.destination = dest;
        self.nodeArray = nodeArray;
        self.orderID = orderID;
    }
    
    return self;
}

- (void)main
{
    __block NSError *error = nil;

    NSDictionary *jsonDictionary = [self getJsonForServerCall];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:kNilOptions error:nil];
    
    NSString *string = URL_STRING;
    
    NSURL *url = [NSURL URLWithString:string];
    
    @try
    {
        NSMutableURLRequest* urlRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
        if (!urlRequest) {
            @throw [NSException exceptionWithName:@"Failed To create URL request" reason:[NSString stringWithFormat:@"Request URL: %@",url] userInfo:nil];
        }
        [urlRequest addValue:CONTENT_TYPE forHTTPHeaderField:@"Content-Type"];
        [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [urlRequest setHTTPMethod:@"POST"];
        [urlRequest setHTTPBody:jsonData];
        
        NSString *userAgent = [NSString stringWithFormat:@"AsthmaBrain Client App %@ / IOS %@ / %@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"],@"1.0",@"iPhone"];
        [urlRequest setValue:userAgent forHTTPHeaderField:@"User-Agent"];
        [urlRequest setNetworkServiceType:NSURLNetworkServiceTypeBackground];
        [urlRequest setHTTPShouldUsePipelining:YES];
        if ([urlRequest respondsToSelector:@selector(setAllowsCellularAccess:)]) {
            [urlRequest setAllowsCellularAccess:YES];
        }
        [urlRequest setHTTPBody:jsonData];
        self.connection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
        
//        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        self.data = [NSMutableData data];
        [self.connection start];
        CFRunLoopRun();
        if (error != nil) {
            if (self.errorBlock != nil) {
                [self performErrorBlock:error];
            }
            return;
        }
        
        if (self.completeBlock!=nil)
        {
            [self performCompletionBlock:self.data];
    
        NSError* errorDict;
        NSDictionary* jsonDictionary = [NSJSONSerialization JSONObjectWithData:self.data
                                                             options:kNilOptions
                                                               error:&errorDict];

        NSLog(@"Response for Updating nodes %@",jsonDictionary);
        }

    }
    
    @catch (NSException *exception)
    {
        self.data = nil;
        if (self.errorBlock!=nil) {
            NSError *errorParam = [NSError errorWithDomain:exception.description code:-1 userInfo:nil];
            [self performErrorBlock:errorParam];
        }

    }
    self.errorBlock = nil;
    self.completeBlock = nil;

}

- (NSError *)getDistanceAndTimeForGivenPath
{
    __block NSError *error = nil;
    __block NSMutableDictionary *resultsDictionary;

    NSDictionary *jsonDictionary = [self getJsonForServerCall];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:kNilOptions error:nil];
    
    NSString *string = URL_STRING;
    
//    NSString *urlString = [string stringByAppendingString:[NSString stringWithFormat:@"%@",jsonData]];

    
//    NSURL *url = [NSURL URLWithString:[string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *url = [NSURL URLWithString:string];

    __block CFRunLoopRef currentThreadref = CFRunLoopGetCurrent();
    CFRunLoopPerformBlock(currentThreadref, kCFRunLoopCommonModes, ^{
        
        NSMutableURLRequest* urlRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
        [urlRequest addValue:CONTENT_TYPE forHTTPHeaderField:@"Content-Type"];
        [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [urlRequest setHTTPMethod:@"POST"];
        [urlRequest setHTTPBody:jsonData];
        
        NSString *userAgent = [NSString stringWithFormat:@"AsthmaBrain Client App %@ / IOS %@ / %@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"],@"1.0",@"iPhone"];
        [urlRequest setValue:userAgent forHTTPHeaderField:@"User-Agent"];
        [urlRequest setNetworkServiceType:NSURLNetworkServiceTypeBackground];
        [urlRequest setHTTPShouldUsePipelining:YES];
        if ([urlRequest respondsToSelector:@selector(setAllowsCellularAccess:)]) {
            [urlRequest setAllowsCellularAccess:YES];
        }
        [urlRequest setHTTPBody:jsonData];
        
//        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:NO];
//        self.completionData = [NSMutableData data];
//        [connection start];
    
         [NSURLConnection sendAsynchronousRequest:urlRequest queue:[[NSOperationQueue alloc] init]  completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
             
             if ([data length]>0 && error == nil)
             {
                 resultsDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
                 NSLog(@"resultsDictionary is %@",resultsDictionary);
                 
             }
             else if ([data length]==0 && error ==nil)
             {
                 NSLog(@" download data is null");
             }
             
             else if( error!=nil)
             {
                 NSLog(@" error is %@",error);

         }
             
         }];
    });
    CFRunLoopWakeUp(currentThreadref);
    CFRunLoopRun();
    return error;
}

//Creating json Object for server call
-(NSMutableDictionary *)getJsonForServerCall
{
    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
    
    NSArray *sourceValues = [self.source componentsSeparatedByString:@","];
    NSArray *destinationValues = [self.destination componentsSeparatedByString:@","];
    
    [jsonDict setObject:[NSNumber numberWithDouble:[[sourceValues firstObject] doubleValue]] forKey:SOURCE_LAT_KEY];
    [jsonDict setObject:[NSNumber numberWithDouble:[[sourceValues lastObject] doubleValue]] forKey:SOURCE_LONG_KEY];
    [jsonDict setObject:[NSNumber numberWithDouble:[[destinationValues firstObject] doubleValue]] forKey:DESTINATION_LAT_KEY];
    [jsonDict setObject:[NSNumber numberWithDouble:[[destinationValues lastObject] doubleValue]] forKey:DESTINATION_LONG_KEY];
    [jsonDict setObject:self.orderID forKey:ORDER_ID];
    
    NSMutableArray *nodeDataArray = [[NSMutableArray alloc] init];
    
    for (NSString *nodeString in self.nodeArray)
    {
        NSArray *nodeLatLongArray = [nodeString componentsSeparatedByString:@","];
        
        NSMutableDictionary *nodeDict = [[NSMutableDictionary alloc] init];
        [nodeDict setObject:[NSNumber numberWithDouble:[[nodeLatLongArray firstObject] doubleValue]] forKey:NODE_LAT];
        [nodeDict setObject:[NSNumber numberWithDouble:[[nodeLatLongArray lastObject] doubleValue]] forKey:NODE_LONG];
        [nodeDataArray addObject:(id)nodeDict];
    }
    
    //Creating JSON string
    [jsonDict setObject:nodeDataArray forKey:NODE_DATA];
    [jsonDict setObject:[NSNumber numberWithBool:YES] forKey:IS_CUSTOMER_ROUTE];
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"json String :%@",jsonString);

    return jsonDict;
    
}

- (void)performErrorBlock:(NSError *)errorParam
{
    if (self.errorBlock)
    {
        // If the source thread is the main thread, perform the selector on the main thread.
        // Otherwise just call the completion block directly
        if ([self.sourceThread isEqual:[NSThread mainThread]]) {
            [self performSelectorOnMainThread:@selector(runErrorBlock:) withObject:errorParam waitUntilDone:YES];
        }
        else {
            self.errorBlock(errorParam);
        }
    }
}

- (void)performCompletionBlock:(id)params
{
    if (self.completeBlock)
    {
        // If the source thread is the main thread, perform the selector on the main thread.
        // Otherwise just call the completion block directly
        if ([self.sourceThread isEqual:[NSThread mainThread]]) {
            [self performSelectorOnMainThread:@selector(runCompletionBlock:) withObject:params waitUntilDone:YES];
        }
        else {
            self.completeBlock(params);
        }
    }
}

- (void)runCompletionBlock:(id)param
{
    if (self.completeBlock)
    {
        self.completeBlock(param);
    }
}

- (void)runErrorBlock:(id)param
{
    if (self.errorBlock)
    {
        self.errorBlock(param);
    }
}

#pragma mark - NSURLConnectionDataDelegate methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    _notifySize = 0;
    [self.data setLength:0];
//    self.error = nil;
//    self.response = response;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    _notifySize += data.length;
    [self.data appendData:data];
    if (_notifySize > _numberOfBytesReceivedBeforeNotifying) {
        _notifySize = 0;
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    CFRunLoopStop(CFRunLoopGetCurrent());
    self.data = nil;
//    self.error = error;
    
//    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    NSLog(@"Download failed! Error - %@ %@",[error localizedDescription], [error userInfo][NSURLErrorFailingURLStringErrorKey]);
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    CFRunLoopStop(CFRunLoopGetCurrent());
    
//    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

@end
