//
//  AppSettings.m
//  Tummy
//
//  Created by Priyanka on 15/05/16.
//  Copyright © 2016 Priyanka. All rights reserved.
//

#import "AppSettings.h"

#define DEVICE_TOKEN                                    @"DeviceToken"
#define ACTIVE_ACCOUNT_ID                               @"ActiveAccountId"
#define LAST_UNFILLED_STATE                             @"LastunfilledView"
#define CONNECTED_DEVICE_UUID                           @"ConnectedDeviceUUID"
#define AUTHENTICATION_TOKEN                            @"AuthenticationToken"
#define RAW_STREAM                                      @"RawStream"
#define CONTROLLER_PROCESSED                            @"ControllerProcessed"

@interface AppSettings ()

@property (nonatomic, strong) NSString *localDeviceToken,*localConnectedDeviceUUID, *localAuthenticationToken, *lastUnfilledView;
@property (nonatomic, strong) NSNumber *localUserId, *rawStream, *controllerProcessed;
@end

@implementation AppSettings

AppSettings *gAppSettings = nil;

+ (AppSettings *)getInstance
{
    if (gAppSettings == nil)
    {
        gAppSettings = [[AppSettings alloc] init];
        
    }
    return gAppSettings;
}

- (instancetype)init
{
    self = [super init];
    
    if (self != nil)
    {
        if([self deviceToken] == nil)
        {
            CFUUIDRef theUUID = CFUUIDCreate(NULL);
            CFStringRef UUIDString = CFUUIDCreateString(NULL, theUUID);
            CFRelease(theUUID);
            [self setDeviceToken:(__bridge_transfer NSString *)UUIDString];
        }
        
    }
    
    return self;
}

#pragma mark - Device Token Info
- (NSString *)deviceToken
{
    if(self.localDeviceToken==nil)
    {
        self.localDeviceToken=[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@",DEVICE_TOKEN]];
    }
    
    return self.localDeviceToken;
}
- (void)setDeviceToken:(NSString *)deviceToken
{
    NSAssert(deviceToken != nil, @"Device ID is NIL!");
    
    [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:[NSString stringWithFormat:@"%@",DEVICE_TOKEN]];
    BOOL success = [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSAssert(success, @"NSUserDefaults Synchronize FAILED!");
    self.localDeviceToken=deviceToken;
}

- (NSNumber *)activeUserId
{
    if(self.localUserId == nil)
        self.localUserId = [[NSUserDefaults standardUserDefaults] objectForKey: ACTIVE_ACCOUNT_ID];
    return self.localUserId;
}

- (void)setActiveUserIdWith:(NSNumber*)userId
{
    [[NSUserDefaults standardUserDefaults] setObject:userId forKey: ACTIVE_ACCOUNT_ID];
      [[NSUserDefaults standardUserDefaults] synchronize];
      self.localUserId = userId;
}
-(void)removeActiveUser
{
    self.localUserId = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey: ACTIVE_ACCOUNT_ID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//Set last unfilled view
- (NSString *)lastUnfilledState
{
    if(self.lastUnfilledView == nil)
        self.lastUnfilledView = [[NSUserDefaults standardUserDefaults] objectForKey: LAST_UNFILLED_STATE];
    return self.lastUnfilledView;
}

- (void)setlastUnfilledState:(NSString *)state
{
    [[NSUserDefaults standardUserDefaults] setObject:state forKey: LAST_UNFILLED_STATE];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.lastUnfilledView = state;
}
-(void)removelastUnfilledState
{
    self.localUserId = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey: LAST_UNFILLED_STATE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}



- (NSString *)connectedDeviceUUID
{
    if(self.localConnectedDeviceUUID == nil)
        self.localConnectedDeviceUUID = [[NSUserDefaults standardUserDefaults] objectForKey: CONNECTED_DEVICE_UUID];
    return self.localConnectedDeviceUUID;
}

- (void)setConnectedDeviceUUIDWithUUID:(NSString*)uuid
{
    [[NSUserDefaults standardUserDefaults] setObject:uuid forKey: CONNECTED_DEVICE_UUID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.localConnectedDeviceUUID = uuid;
}
-(void)removeConnectedDeviceUUID
{
    self.localConnectedDeviceUUID = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey: CONNECTED_DEVICE_UUID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//Authentication token
- (void)setAuthenticationToken:(NSString*)token
{
    [[NSUserDefaults standardUserDefaults] setObject:token forKey: AUTHENTICATION_TOKEN];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.localAuthenticationToken = token;
}

- (NSString *)getAuthToken
{
    if(self.localAuthenticationToken == nil)
        self.localAuthenticationToken = [[NSUserDefaults standardUserDefaults] objectForKey: AUTHENTICATION_TOKEN];
    return self.localAuthenticationToken;
}

-(void)removeAuthToken
{
    self.localAuthenticationToken = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey: AUTHENTICATION_TOKEN];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL)getIsRawStream
{
    if (self.rawStream == nil)
        self.rawStream = [[NSUserDefaults standardUserDefaults] objectForKey: RAW_STREAM];
    return [self.rawStream boolValue];
}

-(void)setIsRawStream:(BOOL)rawStream
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:rawStream] forKey: RAW_STREAM];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.rawStream = [NSNumber numberWithBool:rawStream];
}

-(BOOL)getIsControllerProcessed
{
    if (self.controllerProcessed == nil)
        self.controllerProcessed = [[NSUserDefaults standardUserDefaults] objectForKey: CONTROLLER_PROCESSED];
    return [self.controllerProcessed boolValue];
}

-(void)setIsControllerProcessed:(BOOL)controllerProcessed
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:controllerProcessed] forKey: CONTROLLER_PROCESSED];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.controllerProcessed = [NSNumber numberWithBool:controllerProcessed];
}

@end
