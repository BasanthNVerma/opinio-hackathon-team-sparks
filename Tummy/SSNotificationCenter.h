

#import <Foundation/Foundation.h>

//App state notification macros
#define kNotifyAppStarted                                   @"NotifyAppStarted"
#define kNotifyAppStopped                                   @"NotifyAppStopped"
#define kNotifyAppPaused                                    @"NotifyAppPaused"
#define kNotifyAppResumed                                   @"NotifyAppResumed"

// Workout
#define kNotifyWorkoutStarted                               @"NotifyWorkoutStarted"
#define kNotifyWorkoutStopped                               @"NotifyWorkoutStopped"
#define kNotifyWorkoutEnded                                 @"NotifyWorkoutEnded"

// Network operations notification defines
#define kNotifyNetworkAvailabilityChanged                   @"NotifyNetworkAvailabilityChanged"
#define kNotifyNetworkOperationConnectionReceivedResponse   @"NotifyNetworkOperationConnectionReceivedResponse"
#define kNotifyNetworkOperationConnectionReceivedData       @"NotifyNetworkOperationConnectionReceivedData"
#define kNotifyNetworkOperationConnectionFailed             @"NotifyNetworkOperationConnectionFailed"
#define kNotifyNetworkOperationConnectionFinished           @"NotifyNetworkOperationConnectionFinished"
#define kNotifyErrorNotification                            @"errorMessageNotification"

//Bluetooth connectivity notification defines
#define kNotifyInitStarted                                  @"NotifyBluetoothInitStarted"
#define kNotifyInitSuccess                                  @"NotifyInitSuccess"
#define kNotifyInitFailure                                  @"NotifyInitFailure"
#define kNotifyInitFsSuccess                                @"NotifyInitFsSuccess"
#define kNotifyInitFsReceived                               @"NotifyInitFsReceived"
#define kNotifyBluetoothConnected                           @"NotifyBluetoothConnected"
#define kNotifyBluetoothDisconnected                        @"NotifyBluetoothDisconnected"
#define kNotifyBluetoothPaired                              @"NotifyBluetoothPaired"              
#define kNotifyBluetoothUnpaired                            @"NotifyBluetoothUnpaired"
#define kNotifyBluetoothDeviceNotFound                      @"NotifyBluetoothDeviceNotFound"
#define kNotifyBluetoothDeviceFound                         @"NotifyBluetoothDeviceFound"
#define kNotifyBatteryValueReceived                         @"NotifyBatteryValueReceived"

//Dashboard notifiers for 10 and 60 seconds
#define kNotify10SecondsDone                                @"NotifyTenSecondsOfWorkoutDone"
#define kNotify60SecondsDone                                @"NotifySixtySecondsOfWorkoutDone"

//Value computed notifier
#define kNotifyValueComputed                                @"NotifyValueComputedFromJS"
//Keys used in kNotfiyValueComputed
#define NOTIFICATION_KEY                                    @"notificationKey"
#define NOTIFICATION_VALUE                                  @"notificationValue"
#define NOTIFICATION_COLOR                                  @"notificationColor"
#define NOTIFICATION_TIME_STAMP                             @"notificationTimeStamp"

//Keys for GPS TRacking
#define GPS_TRACKING_KEY                                    @"GPSTrackKey"
#define PREVIOUS_LATITUDE                                   @"previousLatitude"
#define PREVIOUS_LONGITUDE                                  @"previousLongitude"
#define CURRENT_LATITUDE                                    @"currentLatitude"
#define CURRENT_LONGITUDE                                   @"currentLongitude"

//Key for base line coinfig
#define BASELINE_NOTIFRICATION_KEY                          @"baselineNotificationKey"
#define BASELINE_DATA_ARRAY                                 @"baselineDataAray"

@interface SSNotificationCenter : NSObject

+ (void)notify:(NSString *)notification;
+ (void)notify:(NSString *)notification object:(id)obj;
+ (void)notify:(NSString *)notification object:(id)obj userInfo:(NSDictionary *)userInfo;
+ (void)addObserver:(id)observer selector:(SEL)selector name:(NSString *)notification object:(id)obj;
+ (void)removeObserver:(id)observer name:(NSString *)notification object:(id)obj;
+ (void)removeObserver:(id)observer;

@end
