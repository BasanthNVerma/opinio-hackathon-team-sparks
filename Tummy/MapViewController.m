//
//  MapViewController.m
//  Tummy
//
//  Created by Priyanka on 5/14/16.
//  Copyright © 2016 Priyanka. All rights reserved.
//

#import "MapViewController.h"
#import "MapView.h"
#import "UtilityFunctions.h"
#import <CoreLocation/CoreLocation.h>
#import "FetchDistanceAndTimeOperation.h"

@interface MapViewController () <GMSMapViewDelegate,CLLocationManagerDelegate>
@property (nonatomic, strong) MapView *mapview;
@property (nonatomic) BOOL didGetCurLoc;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, strong) UIButton *clearLocationsButton,*sendRouteButton;
@property (nonatomic, strong) NSArray *selectedNodes;
@property (nonatomic, strong)NSArray *latLongsArrayOfDestination;
@property (nonatomic, strong) NSArray *sourceArray;
@end

@implementation MapViewController


-(void)loadView
{
    self.view = [[UIView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64-SCREEN_HEIGHT*0.1)];
    _didGetCurLoc = YES;//change to No, to get curloc
    
    self.sourceArray = [NSArray arrayWithObjects:@"Banashankari",@"Basaveshwar Nagar", @"Hebbal", @"Malleshwaram", @"Vijayanagar, Bangalore", @"Rajajinagar", nil];

    [self clearNodesSelected];
    
    _clearLocationsButton = [[UIButton alloc]initWithFrame:CGRectMake(0, _mapview.frame.size.height+_mapview.frame.origin.y, SCREEN_WIDTH/2, SCREEN_HEIGHT*0.1)];
    [_clearLocationsButton setTitle:@"Clear Route" forState:UIControlStateNormal];
    [_clearLocationsButton addTarget:self action:@selector(clearNodesSelected) forControlEvents:UIControlEventTouchUpInside];
    [_clearLocationsButton setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:214.0/255.0 blue:253.0/255.0 alpha:1]];
    [self.view addSubview:_clearLocationsButton];
    
    _sendRouteButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2, _mapview.frame.size.height+_mapview.frame.origin.y, SCREEN_WIDTH/2, SCREEN_HEIGHT*0.1)];
    [_sendRouteButton setTitle:@"Suggest Route" forState:UIControlStateNormal];
    [_sendRouteButton addTarget:self action:@selector(sendRoute) forControlEvents:UIControlEventTouchUpInside];
    [_sendRouteButton setBackgroundColor:[UIColor colorWithRed:29.0/255.0 green:98.0/255.0 blue:240.0/255.0 alpha:1]];
    [self.view addSubview:_sendRouteButton];
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.distanceFilter = 0;
    self.locationManager.delegate = self;
    
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    //Request permisson form user to access location data.
    [self.locationManager requestAlwaysAuthorization];
    
    if ([CLLocationManager locationServicesEnabled])
    {
        [self.locationManager startUpdatingLocation];
    }
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

}

-(LatLong *) getLocationFromAddressString: (NSString*) addressStr {
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    LatLong *latLong = [[LatLong alloc] init];
    latLong.latitude=latitude;
    latLong.longitude = longitude;
    return latLong;
    
}

#pragma mark -  Button Actions
-(void)clearNodesSelected
{
    CLLocation *curLocation;
    if(self.orderId == 1 || self.orderId == 2)
    {
        LatLong *latLong = [self getLocationFromAddressString:@"Rajajinagar"];
        curLocation = [[CLLocation alloc] initWithLatitude:latLong.latitude longitude:latLong.longitude];
    }
    else if(self.orderId == 4 || self.orderId == 6)
    {
        LatLong *latLong = [self getLocationFromAddressString:@"Banashankari"];
        curLocation = [[CLLocation alloc] initWithLatitude:latLong.latitude longitude:latLong.longitude];
    }
    else if(self.orderId == 3 || self.orderId == 5)
    {
        LatLong *latLong = [self getLocationFromAddressString:@"Malleswaram"];
        curLocation = [[CLLocation alloc] initWithLatitude:latLong.latitude longitude:latLong.longitude];
    }
    
    LatLong *latLong = [self getLocationFromAddressString:[self.sourceArray objectAtIndex:self.orderId-1]];
    CLLocation *sourceLocation = [[CLLocation alloc] initWithLatitude:latLong.latitude longitude:latLong.longitude];
    
    _mapview = [[MapView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64-SCREEN_HEIGHT*0.1) srcLocation:sourceLocation andDestinationLoc:curLocation];
    _mapview.mapView.delegate = self;
    _locationManager.delegate = self;
    [self.view addSubview:_mapview];
}

-(void)sendRoute
{
    LatLong *latLong = [[LatLong alloc]init];
    latLong.latitude = self.mapview.srcLoc.coordinate.latitude;
    latLong.longitude = self.mapview.srcLoc.coordinate.longitude;
    [self.mapview.latLongsArray addObject:latLong];
    
    self.selectedNodes = [[NSArray alloc] initWithArray:self.mapview.latLongsArray];
    
    NSMutableArray *data = [self.selectedNodes mutableCopy];
    NSMutableArray *nodesArray = [[NSMutableArray alloc] init];
    
    for (int a = 1; a < data.count-1; a++)
    {
        [nodesArray addObject:[NSString stringWithFormat:@"%f,%f",[[data objectAtIndex:a] latitude], [[data objectAtIndex:a] longitude]]];
    }
    
    NSString *source = [NSString stringWithFormat:@"%f,%f",[[data firstObject] latitude],[[data lastObject] longitude]];
    NSString *dest = [NSString stringWithFormat:@"%f,%f",[[data lastObject] latitude], [[data lastObject] longitude]];
    
    [self updateServerWithOrderID:[NSString stringWithFormat:@"%ld",self.orderId] source:source  destination:dest andNodeArray:[nodesArray copy]];
    
    if(nodesArray.count == 0)
    {
        UIAlertView *provideNodesAlert = [[UIAlertView alloc] initWithTitle:@"Not Allowed"
                                                               message:@"Select a known shortcut"
                                                              delegate:self
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
        [provideNodesAlert show];
    }
    else
    {
        UIAlertView *validateAlert = [[UIAlertView alloc] initWithTitle:@"Validating!"
                                                                message:@"You shall receive the reward once the route is validated by our team" delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
        validateAlert.tag = 50;
        [validateAlert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 50)
        if(buttonIndex == 0)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
}

#pragma mark - Network call
-(void)updateServerWithOrderID:(NSString *)orderID source:(NSString *)source destination:(NSString *)dest andNodeArray:(NSArray *)nodeArray
{
    FetchDistanceAndTimeOperation *fetchOperation = [[FetchDistanceAndTimeOperation alloc] initWithSource:source Destination:dest OrderID:orderID andNodeArray:nodeArray];
    [fetchOperation start];
}

#pragma mark -  Map
- (NSString *)deviceLocation
{
    NSString *theLocation = [NSString stringWithFormat:@"latitude: %f longitude: %f", _locationManager.location.coordinate.latitude, _locationManager.location.coordinate.longitude];
    return theLocation;
}


-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    double latitude = coordinate.latitude;
    double longitude = coordinate.longitude;
    
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
    [self.mapview addPoint:loc withSource:NO];
    
    //DO SOMETHING HERE WITH THAT INFO
}


//Delegate method to fetch present location
- (void)locationManager:(CLLocationManager* )manager didUpdateLocations:(NSArray<CLLocation* > *)locations
{
    if ( !_didGetCurLoc ) {
        _didGetCurLoc = YES;
        [self.mapview addPoint:[locations lastObject] withSource:YES];
        [self.mapview updateCameraWithLocation:[locations lastObject] enableZoom:YES];
        //Ending location services
        [self.locationManager stopUpdatingLocation];
    }
    
}

//Method to check location fetch data error
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //Error happens when location serviced are available and user given permission to fetch but while fetching error will occur.
    if (error)
    {
    }
}

//Method to check the user permission to fetch location data.
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusDenied)
    {
        //Error happens when user dint give permission to user location services of the device
    }
    
    else if (status == kCLAuthorizationStatusNotDetermined)
    {
        
    }
    else if (status == kCLAuthorizationStatusAuthorizedAlways)
        NSLog(@"Permitted to fetch Geolocation");
}


@end
