//
//  MapView.h
//  Tummy
//
//  Created by Priyanka on 5/15/16.
//  Copyright © 2016 Priyanka. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <GoogleMaps/GoogleMaps.h>

@interface MapView : UIView

@property (nonatomic, strong) GMSMapView *mapView;
@property (nonatomic, strong) GMSPolyline *trackLine;
@property (nonatomic, strong) GMSMutablePath *trackPath;
@property (nonatomic, strong) NSMutableArray *latLongsArray;
@property (nonatomic, strong) CLLocation *srcLoc, *destLoc;

-(instancetype)initWithFrame:(CGRect)frame srcLocation:(CLLocation *)srcLoc andDestinationLoc:(CLLocation *)destLoc;
-(void)addPoint:(CLLocation *)loc withSource:(BOOL)hasSource;
-(void)updateCameraWithLocation:(CLLocation *)location enableZoom:(BOOL)isZoomEnabled;
-(void)createMap;

@end


