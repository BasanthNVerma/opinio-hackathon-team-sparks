
#ifndef AppCommon_h
#define AppCommon_h

//Screen size
#define SCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define SCREEN_BOUNDS [[UIScreen mainScreen]bounds]

#define MINIMUM_HEIGHT_OF_BUTTON        44
#define MINIMUM_WIDTH_OF_BUTTON         44
#define STATUS_BAR_HEIGHT               [UIApplication sharedApplication].statusBarFrame.size.height
#define TABBAR_TITLE_FONT               15

//Macro for setting color using RGBA values
#define COLOR(r,g,b,a)                  [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

#define SSORANGE                        [UIColor colorWithRed:244.0/255.0 green:123.0/255.0 blue:33.0/255.0 alpha:1]

#define GOOGLE_MAPS_KEY                 @"AIzaSyCH4r8QX0ghXqwGi1rw_Ruxt2jWEur9swk"



// Typedefs
typedef void (^CompletionBlockNoParams)();
typedef void (^CompletionBlockParam)(id param);
typedef void (^ErrorBlockParam)(id param);

//CommandQueuePriorities
#define COMMANDQUEUE_PRIORITY_LOW               0
#define COMMANDQUEUE_PRIORITY_MED               1
#define COMMANDQUEUE_PRIORITY_HIGH              2
#define COMMAND_QUEUE_FILE_DATE_FORMATTER       @"yyyy-MM-dd_HH_mm_ss_SSS"
#define COMMAND_FILE_EXTENSION                  @".archive"
#define COMMANDQUEUE_MAX_OPERATIONS             10
#define COMMANDQUEUE_RETRY_COUNT                2

//Custom navigation bar height
#define NAVIGATION_BAR_HEIGHT           64
#define CUSTOM_TAB_BAR_HEIGHT           SCREEN_HEIGHT * 0.09
#define TABBAR_FRAME                    CGRectMake(0, SCREEN_HEIGHT - CUSTOM_TAB_BAR_HEIGHT , SCREEN_WIDTH, CUSTOM_TAB_BAR_HEIGHT)

#define HH_MM_SS                        @"HH:mm:ss"
#define HH_MM_SS_DEFAULT                @"00:00:00"

#define PAGE_CONTROL_HEIGHT             0.02717 * SCREEN_HEIGHT

#define HEART_RATE_SERVICE                          @"180D"

#define HEART_RATE_NOTIFY_CHARACTERISTIC            @"2A37" //For Unacknowledged reads (Fast and unreliable)
#define HEART_RATE_INDICATIONS_CHARACTERISTIC       @"2A38" //For acknowledged reads (Slow and Reliable)
#define HEART_RATE_WRITE_CHARACTERISTIC             @"2A39" //For init and Controller receiver events
#define CLOSE                                       NSLocalizedString(@"Close",@"Close Label")
#define CANCEL                                      NSLocalizedString(@"Cancel",@"Cancel Text")
#define OK                                          NSLocalizedString(@"OK",@"Ok Text")
#define INITIAL_BIOMETRIC_VALUE                     @"- -"

#define NO_INTERNET_MESSAGE                         NSLocalizedString(@"No Internet available", @"No internet available message")
//Biometric paramaters units
#define UNIT_RESPIRATORY_RATE @"breath/min"
#define UNIT_HEART_RATE @"bpm"
#define UNIT_BP_HEART_RATE_CAPS @"BPM"
#define UNIT_MINUTE             @"MS"
#define UNIT_LBS                @"LBS"

#define UNIT_CALORIE_BURN_RATE @"cal"
#define UNIT_DISTANCE @"mi"

//Network call constants
#define SUCCES_CODE                                 200
#define UN_AUTHERIZED_CODE                          401
#define CODE                                        @"code"
#define ERROR                                       @"error"
#define MESSAGE                                     @"message"
#define ERROR_MESSAGE                               @"errorMessage"
#define AUTHENTICATION_FAILED_TEXT                  NSLocalizedString(@"Authentication failed! Login again!",@"Authentication failed text")

//Notification keys
#define RESPIRATION_RATE                @"respirationRate"
#define MUSCLE_STRENGTH                 @"muscleStrength"
#define HEART_RATE                      @"heartRate"
#define BODY_TEMPERATURE                @"bodyTemperature"
#define MUSCLE_STRENGTH_CHEST           @"muscleStrengthChest"
#define MUSCLE_STRENGTH_CORE            @"muscleStrengthCore"
#define MUSCLE_STRENGTH_SHOULDER        @"muscleStrengthShoulder"
#define MUSCLE_STRENGTH_BACK            @"muscleStrengthBack"
#define HEART_RATE_ZONE                 @"heartRateZone"

#define GRAPHCELL_HEIGHT                self.frame.size.height * 0.65
#define NORMALCELL_HEIGHT               SCREEN_HEIGHT/10
#define HOME_LABEL                      NSLocalizedString(@"Home",@"Home label")
#define WORKOUTS_LABEL                  NSLocalizedString(@"Workouts:",@"Workouts label")
#define BIOMETRIC_LABEL                 @"BiometricParameter"
#define MUSCLE_GRAPH_MODE_LABEL         @"muscleModeGraph"


#define BIOMETRIC_PARAMETER_CELL_WITHOUT_VALUE        @"BiometricParameterCellWithoutValue"
#define BIOMETRIC_PARAMETER_CELL                        @"BiometricParameterCell"

#define GRAPH_CELL                      @"GraphCell"
#define HEADER_CELL                     @"HeaderCell"
#define NUM_BASED_WORKOUT               @"NumBasedWorkout"
#define MUSCLE_GRAPH_MODE               @"muscleModeGraph"
#define MUSCLE_MODE_BIO_PARA_TILES      @"muscleModeBioParaTiles"

//Menutabbar
#define LIST_TYPE                       @"ListType"
#define NAVIGATION_TYPE                 @"NavigationType"

#define OUTPUTKEY_TEXT                         @"outputKey"
#define COLOR_TEXT                             @"color"

#define CHEST                           NSLocalizedString(@"Chest",@"Chest label")
#define CORE                            NSLocalizedString(@"Core",@"Core label")
#define SHOULDER                        NSLocalizedString(@"Shoulder",@"Shoulder label")
#define BACK                            NSLocalizedString(@"Back",@"Back label")
#define MILES                           NSLocalizedString(@"Miles",@"Miles label")
#define STEPS                           NSLocalizedString(@"Steps",@"Steps label")
#define CAL                             NSLocalizedString(@"Cal",@"Cal label")

#define USER_SETTINGS_CELL_TITLE               NSLocalizedString(@"User Settings", @"User Settings cell text")
#define FEEDBACK_CELL_TITLE                    NSLocalizedString(@"Feedback", @"User Settings cell text")
#define DEVICE_CELL_TITLE                      NSLocalizedString(@"Device", @"User Settings cell text")
#define GLOSSARY_CELL_TITLE                    NSLocalizedString(@"Glossary", @"User Settings cell text")
#define USAGE_WARNING_CELL_TITLE              NSLocalizedString(@"Usage Warning", @"User Settings cell text")
#define ABOUT_US_CELL_TITLE                    NSLocalizedString(@"About US", @"User Settings cell text")
#define EXPLORE_THE_APP_CELL_TITLE             NSLocalizedString(@"Explore the App", @"User Settings cell text")
#define SIGN_OUT_CELL_TITLE                    NSLocalizedString(@"Sign Out", @"User Settings cell text")
#define CANCEL_ACCOUNT_CELL_TITLE              NSLocalizedString(@"Cancel Account", @"User Settings cell text")

#define FITNESS_TITLE_TEXT                NSLocalizedString(@"Fitness", @"Fitness title")
#define WORKOUT_TITLE_TEXT                NSLocalizedString(@"Workout", @"Workout title")
#define MUSCLE_TITLE_TEXT                 NSLocalizedString(@"Muscle", @"Muscle title")
#define CARDIAC_TITLE_TEXT                NSLocalizedString(@"Cardiac", @"Cardiac title")
#define RESPIRATORY_TITLE_TEXT            NSLocalizedString(@"Respiratory", @"Respiratory title")
#define HEART_TITLE_TEXT                  NSLocalizedString(@"Heart",@"Heart label")


#define HEART_RATE_CELL_LABEL                                           NSLocalizedString(@"Heart rate",@"Heart rate Label")
#define STROKE_VOLUME_CELL_LABEL                                        NSLocalizedString(@"Stroke Volume",@"Stroke Volume Label")
#define RESPIRATORY_RATE_CELL_LABEL                                     NSLocalizedString(@"Respiratory Rate",@"Respiratory Rate Label")
#define TIDAL_VOLUME_CELL_LABEL                                         NSLocalizedString(@"Tidal Volume",@"Tidal Volume Label")
#define SKIN_TEMP_CELL_LABEL                                            NSLocalizedString(@"Skin Temp (F)",@"Skin Temp  Label")
#define EXCITEMENT_CELL_LABEL                                           NSLocalizedString(@"Excitement",@"Excitement Label")
#define CALORIES_BURNED_CELL_LABEL                                      NSLocalizedString(@"Calories Burned",@"Calories Burned Label")
#define STEPS_CELL_LABEL                                                NSLocalizedString(@"Steps",@"Steps Label")
#define DISTANCE_CELL_LABEL                                             NSLocalizedString(@"Distance (mi)",@"Distance Label")
#define ASTHMA_ALERT_CELL_LABEL                                         NSLocalizedString(@"Asthma Alert",@"Asthma Alert label text")

//TREND GRAPH
#define VO2max_ZONE                                                     NSLocalizedString(@"VO2max zone", @"VO2max zone label")
#define HEART_RATE_RECOVERY                                             NSLocalizedString(@"Heart rate recovery", @"Heart rate recovery label")
#define HEART_RATE_ZONE_CELL_LABEL                                      NSLocalizedString(@"Heart rate zone",@"Heart rate zone label")
#define HEART_RATE_VARIABILITY                                          NSLocalizedString(@"Heart rate variablity", @"Heart rate variablity label")

#define AEROBIC_FITNESS                                                 NSLocalizedString(@"Aerobic fitness", @"Aerobic fitness label")
#define STRENGTH                                                        NSLocalizedString(@"Strength", @"Strength label")
#define AEROBIC_CAPACITY                                                NSLocalizedString(@"Aerobic capacity", @"Aerobic capacity label")
#define RHYTHM_INDEX                                                    NSLocalizedString(@"Rhythm Index", @"Rhytm Index label")
#define CARDIAC_PERFORMANCE                                             NSLocalizedString(@"Cardiac performance", @"Cardiac performance label")
#define RESPIRATORY_PERFORMANCE                                         NSLocalizedString(@"Respiratory performance", @"Respiratory performance label")
#define CALORIES_BURNED                                                 NSLocalizedString(@"Calories burned", @"Calories burned label")
#define HYDRATION                                                       NSLocalizedString(@"Hydration", @"Hydration label")
#define DISTANCE_MILES                                                  NSLocalizedString(@"Distance (miles)", @"Distance (miles) label")
#define BODY_TEMPERATURE_F                                              NSLocalizedString(@"Body temperature (F)", @"Body temperature (F) label")
#define SYMMETRY                                                        NSLocalizedString(@"Symmetry", @"Symmetry label")


#define UPPER_ARM                                                       NSLocalizedString(@"Upper Arm", @"Upper Arm label")
#define LOWER_ARM                                                       NSLocalizedString(@"Lower Arm", @"Lower Arm label")

#define TITLE_MUSCLE_EFFORT                                             NSLocalizedString(@"Muscle Effort",@"Title of the Muscle Effort biometric paramter view")


#define PECS                                                            NSLocalizedString(@"Pecs", @"Pecs label")
#define DELTOID                                                         NSLocalizedString(@"Deltoids", @"Deltoid label")
#define TRAPS                                                           NSLocalizedString(@"Traps", @"Traps label")
#define LATS                                                            NSLocalizedString(@"Lats", @"Lats label")
#define BICEP                                                           NSLocalizedString(@"Biceps", @"Bicep label")
#define TRICEP                                                          NSLocalizedString(@"Triceps", @"Tricep label")
#define EXTENSOR                                                        NSLocalizedString(@"Extensors", @"Extensor label")
#define FLEXOR                                                          NSLocalizedString(@"Flexors", @"Flexor label")



#define TODAY                               NSLocalizedString(@"Today", @"Today label")
#define FIVE                                NSLocalizedString(@"5", @"5 label")
#define TEN                                 NSLocalizedString(@"10", @"10 label")
#define FIFTEEN                             NSLocalizedString(@"15", @"15 label")
#define TWENTY                              NSLocalizedString(@"20", @"20 label")
#define TWENTY_FIVE                         NSLocalizedString(@"25", @"25 label")

//Main menubar macros
#define DASHBOARD_LABEL                 NSLocalizedString(@"Dashboard", @"Dashboards label")
#define TREND_GRAPHS_LABEL              NSLocalizedString(@"Trend", @"TrendGraphs label")
#define PROFILE_LABEL                   NSLocalizedString(@"Profile", @"Profile label")
#define WORKOUTS_LOCAL_LABEL            NSLocalizedString(@"Workouts", @"Workouts label")


#define PERIPHERAL_TEXT                   @"peripheral"
#define RSSI_TEXT                         @"RSSI"

//For trend graphs
#define  WEEKS                            @"Weeks"
#define  MONTHS                           @"Months"
#define  YEARS                            @"Years"

#define  WEEKS_DAYS                       7
#define  MONTHS_DAYS                      30
#define  YEARS_DAYS                       356


#define SETTINGS_USER_INDEX_BUTTON_1_TAG    1001
#define SETTINGS_USER_INDEX_BUTTON_2_TAG    1002
#define SETTINGS_USER_INDEX_BUTTON_3_TAG    1003
#define SETTINGS_USER_INDEX_BUTTON_4_TAG    1004
#define SETTINGS_USER_INDEX_BUTTON_5_TAG    1005

//Heart rate zone and VO2 max tags
#define RESET_IMAGE_VIEWS_TAG                     1006
#define COOLDOWN_IMAGE_VIEW_TAG                   1007
#define FATBURN_IMAGE_VIEW_TAG                    1008
#define CALORIESBURNED_IMAGE_VIEW_TAG             1009
#define HIGHCALORIESBURNED_IMAGE_VIEW_TAG         1010
#define MAXIMUM_IMAGE_VIEW_TAG                    1011

//Quickstart view page Indicator tags
#define QUICK_START_PAGE_INDICATOR_TAG_1          1021
#define QUICK_START_PAGE_INDICATOR_TAG_2          1022
#define QUICK_START_PAGE_INDICATOR_TAG_3          1023
#define QUICK_START_PAGE_INDICATOR_TAG_4          1024
#define QUICK_START_PAGE_INDICATOR_TAG_5          1025
#define QUICK_START_PAGE_INDICATOR_TAG_6          1026
#define QUICK_START_PAGE_INDICATOR_TAG_7          1027
#define QUICK_START_PAGE_INDICATOR_TAG_8          1028

#define MAX_RECONNECTING_COUNT                     2


#define DD_MM_YYYY_HH_MM        @"dd-MM-yyyy, hh:mm"
#define NOT_YET_SYNCED_LABEL    @"--/--/--"

//Dashboard 3 dropdown
#define TARGET_HEART_RATE_ZONE          NSLocalizedString(@"Target Heart rate Zone",@"Dropdown button label for target heart rate zone")
#define VO2_MAX                         NSLocalizedString(@"% VO2max",@"Dropdown button label for VO2MAX")

//Device screen
#define VERSION_IDENTIFIER                  @"Version identifier"
#define NORMAL_DEVICE_SETTING_IDENTIFIER    @"Normal Device setting identifier"
#define NO_DEVICE_CONNECTED                 NSLocalizedString(@"No Device Connected",@"No Device Connected Label")

#define CONTROLLER_SIGNAL_STRENGTH_KEY      NSLocalizedString(@"Controller Signal Strength",@"Controller Signal Strength key")
#define SYNCED_DATE_KEY                     NSLocalizedString(@"Synced date:",@"Synced date key")
#define FIRMWARE_VERSION_KEY                NSLocalizedString(@"Firmware ver:",@"Firmware ver key")
#define APPLICATION_VERSION_KEY             NSLocalizedString(@"App ver:",@"App ver key")
#define DEVICE_BATTERY_KEY                  NSLocalizedString(@"Device battery:",@"Device battery key")
#define CURRENT_DEVICE_KEY                  NSLocalizedString(@"Current Device:",@"Current Device key")

#define CF_BUNDLE_SHORT_VERSION_STRING      @"CFBundleShortVersionString"

//Device selection alert text
#define DEVICE_RECONNECTION_ON_WORKOUT  NSLocalizedString(@"Bluetooth device got disconnected! Do you want to reconnect or stop the workout",@"Device reconnection while workout label")
#define RECONNECT_LABEL                 NSLocalizedString(@"Reconnect",@"Reconnect label")
#define DEVICE_RECONNECTION_LABEL  NSLocalizedString(@"Bluetooth device got disconnected!",@"Device reconnection label")

#define SELECT_DEVICE_ALERT_LABEL  NSLocalizedString(@"You have not connected with any bluetooth device, select the device",@"Select device label")

#define FAILED_CONNECTION_LABEL  NSLocalizedString(@"Connecting bluetooth device failed, try to connect new device",@"Failed connection label")
#define FAILED_RECONNECTION_LABEL  NSLocalizedString(@"Connection with SmartShirt Lost. Will reconnect once SmartShirt is back in range.",@"Failed reconnection label")
#define CONNECTING_BLUETOOTH_DEVICE_LABEL  NSLocalizedString(@"Connecting to bluetooth device",@"Connecting bluetooth device label")
#define RECONNECTING_BLUETOOTH_DEVICE_LABEL  NSLocalizedString(@"Device got disconnected, Reconnecting..",@"Connecting bluetooth device label")


#define INTERNET_CONNECTION_APPEARS_TO_BE_OFFLINE    NSLocalizedString(@"The Internet connection appears to be offline.",@"Internet connection appears to be offline text")
#define FETCH_DATA_FAILED_MESSAGE              NSLocalizedString(@"Fetch request failed, try again!", @"Data fetch failed message")
#define NO_DATA_IN_SERVER                       NSLocalizedString(@"No data in server", @"No data in server message")



#define US_LOCALE                                   @"US"

#define DOB_JSON_KEY                    @"dob"
#define HEIGHT_JSON_KEY                 @"height"
#define WEIGHT_JSON_KEY                 @"weight"
#define GENDER_JSON_KEY                 @"gender"
#define PHONE_NUMBER_JSON_KEY           @"phone"
#define USER_ID_JSON_KEY                @"userId"
#define USER_JSON_KEY                   @"user"
#define USER_PROFILE_ID_KEY             @"userProfileId"
#define USER_FIRST_NAME                 @"firstName"
#define USER_LAST_NAME                  @"lastName"

#define USER_ID                         @"userId"

#define MENU_TABLE_PARENT_CELL          @"ParentCellIdentifier"
#define MENU_TABLE_CHILD_CELL           @"ChildCellIdentifier"

#define ADD_ACTION                      @"AddAction"
#define REMOVE_ACTION                   @"RemoveAction"

//used in case no Accordion Graph is shown
#define NO_CELL_SELECTED 100000
#define DB_CV_GRAPH_CELL_IDENTIFIER                         @"collectionViewGraphCellID"
#define DB_CV_BIOMETRIC_PARAMETER_CELL_IDENTIFIER           @"collectionViewBiometricParameterCellID"
#define DB_CV_PERCENTAGE_ZONES_CELL_IDENTIFIER              @"collectionViewPercentZoneParameterCellID"
#define DB_CV_BIOMETRIC_PARAMETER_ARROW_CELL_IDENTIFIER     @"collectionViewBiometricParamaterArrowCellID"
#define DB_CV_MUSCLE_CELL_IDENTIFIER                        @"collectionViewMuscleCellID"
#define DB_CV_MUSCLE_AND_PROGRESS_BAR_CELL_IDNETIFIER       @"collectionViewMuscleAndProgressBarCellID"
#define DB_CV_MAP_CELL_IDENTIFIER                           @"collectionViewMapCellID"

#define DB_CV_CELL_TYPE_GRAPH                               @"CVGraphCell"
#define DB_CV_CELL_TYPE_BIOMETRIC_PARAMETER                 @"CVBiometricParameterCell"
#define DB_CV_CELL_TYPE_PERCENT_ZONES                       @"CVPercentZonesCell"
#define DB_CV_CELL_TYPE_BIOMETRIC_PARAMTER_WITH_ARROW       @"CVBiometricParamterCellWithArrow"
#define DB_CV_CELL_TYPE_MUSCLE                              @"CVMuscleCell"
#define DB_CV_CELL_TYPE_MUSCLE_AND_PROGRESS_BAR             @"CVMuscleAndProgressBarCell"
#define DB_CV_CELL_TYPE_MAP                                 @"CVMapCell"
//Image names in macro
#define MUSCLE_FRONT_OUTLINE_IMAGE_NAME                 @"SSMOutlineFront"
#define MUSCLE_FRONT_DELTOID_LEFT_IMAGE_NAME            @"SSMFrontDeltoidLeft"
#define MUSCLE_FRONT_DELTOID_RIGHT_IMAGE_NAME           @"SSMFrontDeltoidRight"
#define MUSLE_LEFT_ARM_EXTENSOR_IMAGE_NAME              @"SSMleftArmExtensor"
#define MUSCLE_RIGHT_ARM_EXTENSOR_IMAGE_NAME            @"SSMRightArmExtensor"
#define MUSCLE_BICEP_LEFT__IMAGE_NAME                   @"SSMBicepsLeft"
#define MUSCLE_BICEP_RIGHT_IMAGE_NAME                   @"SSMBicepsRight"
#define MUSCLE_PEC_LEFT_IMAGE_NAME                      @"SSMPectLeft"
#define MUSCLE_PEC_RIGHT_IMAGE_NAME                     @"SSMPectRight"
#define MUSCLE_UPPER_CORE_IMAGE_NAME                    @"SSMUpperCore"
#define MUSCLE_LOWER_CORE_IMAGE_NAME                    @"SSMLowerCore"
#define MUSCLE_OUTLINE_BACK_IMAGE_NAME                  @"SSMOutlineBack"
#define MUSCLE_BACK_DELTOID_LEFT_IMAGE_NAME             @"SSMBackDeltoidLeft"
#define MUSCLE_BACK_DELTOID_RIGHT_IMAGE_NAME            @"SSMBackDeltoidRight"
#define MUSCLE_LEFT_UPPER_BACK_IMAGE_NAME               @"SSMLeftUpperBack"
#define MUSCLE_RIGHT_UPPER_BACK_IMAGE_NAME              @"SSMRightUpperBack"
#define MUSCLE_LEFT_LATS_IMAGE_NAME                     @"SSMLeftLats"
#define MUSCLE_RIGHT_LATS_IMAGE_NAME                    @"SSMRightLats"
#define MUSCLE_LEFT_TRICEP_IMAGE_NAME                   @"SSMLeftTricep"
#define MUSCLE_RIGHT_TRICEP_IMAGE_NAME                  @"SSMRightTricep"
#define MUSCLE_LEFT_FLEXORS_IMAGE_NAME                  @"SSMLeftFlexors"
#define MUSCLE_RIGHT_FLEXORS_IMAGE_NAME                 @"SSMRightFlexors"

//Image names in macro
#define MUSCLE_FRONT_OUTLINE_IMAGE_NAME                 @"SSMOutlineFront"
#define MUSCLE_FRONT_DELTOID_LEFT_IMAGE_NAME            @"SSMFrontDeltoidLeft"
#define MUSCLE_RIGHT_FLEXORS_IMAGE_NAME                 @"SSMRightFlexors"


//Main menubar UIImages
#define MENU_DASH_BOARD_IMAGE_ON_ACTIVE_MODE                 [UIImage imageNamed:@"TabIconDashboardActive"]
#define MENU_DASH_BOARD_IMAGE_ON_INACTIVE_MODE               [UIImage imageNamed:@"TabIconDashboard"]

#define MENU_TREND_IMAGE_ON_ACTIVE_MODE                      [UIImage imageNamed:@"TabIconTrendActive"]
#define MENU_TREND__IMAGE_ON_INACTIVE_MODE                   [UIImage imageNamed:@"TabIconTernd"]

#define MENU_PROFILE_IMAGE_ON_ACTIVE_MODE                    [UIImage imageNamed:@"TabIconProfileActive"]
#define MENU_PROFILE_IMAGE_ON_INACTIVE_MODE                  [UIImage imageNamed:@"TabIconProfile"]

#define MENU_WORKOUT_IMAGE_ON_ACTIVE_MODE                    [UIImage imageNamed:@"TabIconWorkoutsActive"]
#define MENU_WORKOUT_IMAGE_ON_INACTIVE_MODE                  [UIImage imageNamed:@"TabIconWorkouts"]

//Episode Demarcation states
#define EPISODE_DEMARCATION_STATE_START     1
#define EPISODE_DEMARCATION_STATE_STOP      0
#define EPISODE_DEMARCATION_STATE_PAUSE     2
#define EPISODE_DEMARCATION_STATE_RESUME    1

#endif /* AppCommon_h */
