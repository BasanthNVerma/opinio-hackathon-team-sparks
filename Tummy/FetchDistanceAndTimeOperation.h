//
//  FetchDistanceAndTimeOperation.h
//  NetworkOp
//
//  Created by Sunil Rao on 14/05/16.
//  Copyright © 2016 Sunil Rao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FetchDistanceAndTimeOperation : NSOperation

- (id)initWithSource:(NSString *)source Destination:(NSString *)dest OrderID:(NSString *)orderID andNodeArray:(NSArray *)nodeArray;

@end