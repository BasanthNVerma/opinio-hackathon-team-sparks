//
//  MenuController.m
//  Tummy
//
//  Created by Priyanka on 5/14/16.
//  Copyright © 2016 Priyanka. All rights reserved.
//

#import "MenuController.h"
#import "LatLong.h"
#import "MapViewController.h"
#import "FetchDistanceAndTimeOperation.h"

@interface MenuController ()
@property (nonatomic, strong)NSArray *menuArray;
@property (nonatomic, strong)NSArray *sourceArray;
@property (nonatomic, strong)MapViewController *mapViewController;
@property (nonatomic) NSUInteger selectedCell;
@end

@implementation MenuController

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"Place Order";
}
-(void) loadView
{
    self.tableView = [[UITableView alloc] init];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.view = self.tableView;
    self.menuArray = [NSArray arrayWithObjects:@"Order-1",@"Order-2",@"Order-3",@"Order-4",@"Order-5",@"Order-6", nil];
    self.sourceArray = [NSArray arrayWithObjects:@"Banashankari",@"Basaveshwar Nagar",@"Hebbal",@"Malleshwaram",@"Vijayanagar",@"Rajajinagar", nil];
    [UINavigationBar appearance].translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:52.0/255.0 green:170.0/255.0 blue:220.0/255.0 alpha:1];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.menuArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:24];
    cell.textLabel.text = [self.menuArray objectAtIndex:indexPath.row];
    cell.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Destination-%@",[self.sourceArray objectAtIndex:indexPath.row]];
    cell.detailTextLabel.textColor = [UIColor colorWithRed:52.0/255.0 green:170.0/255.0 blue:220.0/255.0 alpha:1];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIAlertView *showAlertView = [[UIAlertView alloc]
                                        initWithTitle:@"Know a shortcut? Share it for rewards"
                                        message:nil
                                        delegate:self
                                        cancelButtonTitle:nil
                                        otherButtonTitles :@"Share It",@"No:(",nil];
    [showAlertView show];
    
    self.selectedCell = indexPath.row;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    self.mapViewController = [[MapViewController alloc] init];
    if(buttonIndex == 0)
    {
        //get the map here
        self.navigationItem.title = @"Back";
        [self.navigationController pushViewController:self.mapViewController animated:YES];
        
        self.mapViewController.orderId = self.selectedCell+1;
    }
    
    if(buttonIndex == 1)
    {        
        LatLong *destLatlong = [[LatLong alloc] init];
        destLatlong = [self.mapViewController getLocationFromAddressString:[self.sourceArray objectAtIndex:self.selectedCell]];
        NSString *dest = [NSString stringWithFormat:@"%f,%f",[destLatlong latitude], [destLatlong longitude]];
        
        LatLong *sourceLatlong = [[LatLong alloc] init];
        sourceLatlong = [self.mapViewController getLocationFromAddressString:@"Malleswaram"];
        NSString *source = [NSString stringWithFormat:@"%f,%f",[sourceLatlong latitude], [sourceLatlong longitude]];
        
        [self.mapViewController updateServerWithOrderID:[NSString stringWithFormat:@"%ld",self.selectedCell+1] source:source  destination:dest andNodeArray:nil];
        
    }
}
@end

