
#import "UtilityFunctions.h"

#define SCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

#define FINISHING_AUTHENTICATION        NSLocalizedString(@"Almost done.",@"Message Displayed on progress view while finishing authentication")
#define AUTHENTICATING_WITH_CONTROLLER  NSLocalizedString(@"Authenticating your SmartCore.",@"Message displayed on progress view while authentication")
#define ALERT_BOX_TITLE_AUTH_FAILED     NSLocalizedString(@"Authentication failed.",@"Title of the SSAlertBox displayed when init authentication fails")
#define KEY_TEXT                        @"key"
#define VALUE_TEXT                      @"value"

UtilityFunctions *sharedUtilityFunction;

@interface UtilityFunctions ()
@property (nonatomic, strong) NSTimer *loadingTimer;
@property (nonatomic, strong) UIView *loadingView;
@property (nonatomic, strong) UIView *progressView;
@end


@implementation UtilityFunctions

+(UtilityFunctions*)sharedInstance
{
    if (sharedUtilityFunction==nil) {
        sharedUtilityFunction = [[UtilityFunctions alloc]init];
    }
    return sharedUtilityFunction ;
}
-(instancetype)init
{
    self = [super init];
    if(self)
    {
    }
    return self;
}
#pragma mark - Shared Appdelegate
//Shared appdelegate
+ (AppDelegate *)sharedAppDelegate
{
    //Will return shared appDelegate object
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

#pragma mark - Unique UUID
+ (NSString *)getUUID
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge_transfer NSString *)string;
}

#pragma mark - Form validation methods
// Method to validate email address format
+(BOOL)validateEmail:(NSString *)emailText
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailText];
}

#pragma mark - Create Custom Activity Indicator
+(UIView *)getAcivityIndicaterWithMessage:(NSString *)message
{
    /*View which contais activity indicator.*/
    UIView *activityIndicatorView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 120)];
    activityIndicatorView.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
    activityIndicatorView.backgroundColor = [UIColor clearColor];
    
    /*Background view of activity indicator.*/
    UIView *backgroundView = [[UIView alloc]initWithFrame:CGRectMake((activityIndicatorView.frame.size.width/2-activityIndicatorView.frame.size.height/4), (activityIndicatorView.frame.size.height/2-activityIndicatorView.frame.size.height/4), activityIndicatorView.frame.size.height/2, activityIndicatorView.frame.size.height/2)];
    backgroundView.backgroundColor=[UIColor grayColor];
    backgroundView.layer.cornerRadius=15;
    
    /*Activity indicator for view.*/
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0, 0, backgroundView.frame.size.width, backgroundView.frame.size.height)];
    activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [activityView startAnimating];
    
    [backgroundView addSubview:activityView];
    
    [activityIndicatorView addSubview:backgroundView];
    
    /*If message is nill then no need of allocation.*/
    if ((message != nil) && (![message isEqualToString:@""]))
    {
        /*Message label for description of web page.*/
        UILabel *messageLabel;
        messageLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, activityIndicatorView.frame.size.height/2)];
        messageLabel.center = CGPointMake(backgroundView.center.x, backgroundView.center.y+backgroundView.frame.size.height);
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.textColor = [UIColor whiteColor];
        messageLabel.text = message;
        [activityIndicatorView addSubview:messageLabel];
    }
    return activityIndicatorView;
}

#pragma mark - Age from DOB
+(NSInteger)getAgeFromDOB:(NSDate *)dob
{
    
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:dob
                                       toDate:now
                                       options:0];
    NSInteger age = [ageComponents year];
    return age;
}

#pragma mark - color
+(UIColor *)colorFromHexString:(NSString *)hexString
{
    
    unsigned rgbvalue = 5545545;
    NSScanner *scanner= [NSScanner scannerWithString:hexString ];
    [scanner scanHexInt:&rgbvalue];

    return [ UIColor colorWithRed:((rgbvalue & 0xFF3000) >> 16)/255.0 green:((rgbvalue & 0xFF334400 ) >> 8)/255.0 blue:(rgbvalue & 0xFF)/255.0 alpha:1.0];
}
//Check Stack of ViewController
+(UIViewController *)checkViewControllerPresence:(id)viewControllerClass
{
    NSArray *noOfViewControllers = [self.sharedAppDelegate.navigation viewControllers];
    for (UIViewController* arrayObj in noOfViewControllers)
    {
        if ([arrayObj isKindOfClass:viewControllerClass])
        {
            return arrayObj;
        }
    }
    return nil;
}

@end
