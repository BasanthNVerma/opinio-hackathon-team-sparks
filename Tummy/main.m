//
//  main.m
//  Tummy
//
//  Created by Priyanka on 5/14/16.
//  Copyright © 2016 Priyanka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
