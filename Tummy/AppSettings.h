//
//  AppSettings.h
//  Tummy
//
//  Created by Maheswar on 12/01/16.
//  Copyright © 2016 Priyanka. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppSettings : NSObject
+ (AppSettings *)getInstance;

//Device Token
- (NSString *)deviceToken;
- (void)setDeviceToken:(NSString *)deviceToken;

- (NSNumber *)activeUserId;
- (void)setActiveUserIdWith:(NSNumber*)userId;
- (void)removeActiveUser;

- (NSString *)lastUnfilledState;
- (void)setlastUnfilledState:(NSString *)state;
- (void)removelastUnfilledState;

//To remember already connected device
- (NSString *)connectedDeviceUUID;
- (void)setConnectedDeviceUUIDWithUUID:(NSString*)uuid;
- (void)removeConnectedDeviceUUID;

//To store authtoken for active user
- (void)setAuthenticationToken:(NSString*)token;
- (NSString *)getAuthToken;

//To store bit flag for raw data streaming
-(BOOL)getIsRawStream;
-(void)setIsRawStream:(BOOL)rawStream;

//To store bit flag for raw data streaming
-(BOOL)getIsControllerProcessed;
-(void)setIsControllerProcessed:(BOOL)controllerProcessed;

@end
