

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface UtilityFunctions : NSObject

///Shared instance of UtilityFunctions class
+(UtilityFunctions*)sharedInstance;

///Function to get unique UUID
+ (NSString *)getUUID;

///Shared appdelegate
+ (AppDelegate *)sharedAppDelegate;

///Method to validate email address format
+(BOOL)validateEmail:(NSString *)emailText;

///Creates a activity indicater along with the passed string
+(UIView *)getAcivityIndicaterWithMessage:(NSString *)message;

///Returns Age of type NSInteger for DOB
+(NSInteger)getAgeFromDOB:(NSDate *)dob;

///Return UIColor for color value in Hexadecimal String
+(UIColor *)colorFromHexString:(NSString *)hexString;

///Check Stack of ViewController
+(UIViewController *)checkViewControllerPresence:(id)viewControllerClass;
@end
