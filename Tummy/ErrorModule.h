//
//  ErrorModule.h
//  Tummy
//
//  Created by Priyanka on 15/05/16.
//  Copyright © 2016 Priyanka. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ERROR_UNKNOWN_ERROR     1

@interface ErrorModule : NSObject

+ (NSError *)errorWithErrorCode:(NSInteger)code;

+ (NSError *)errorWithServerErrorCode:(NSString *)string;

@end
