

#import "SSNotificationCenter.h"

@implementation SSNotificationCenter

+ (void)notify:(NSString *)notification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:notification object:nil userInfo:nil];
}

+ (void)notify:(NSString *)notification object:(id)obj
{
    [[NSNotificationCenter defaultCenter] postNotificationName:notification object:obj userInfo:nil];
}

+ (void)notify:(NSString *)notification object:(id)obj userInfo:(NSDictionary *)userInfo
{
    [[NSNotificationCenter defaultCenter] postNotificationName:notification object:obj userInfo:userInfo];
}

+ (void)addObserver:(id)observer selector:(SEL)selector name:(NSString *)notification object:(id)obj
{
    [[NSNotificationCenter defaultCenter] addObserver:observer selector:selector name:notification object:obj];
}

+ (void)removeObserver:(id)observer name:(NSString *)notification object:(id)obj
{
    [[NSNotificationCenter defaultCenter] removeObserver:observer name:notification object:obj];
}

+ (void)removeObserver:(id)observer
{
    [[NSNotificationCenter defaultCenter] removeObserver:observer];
}

@end
