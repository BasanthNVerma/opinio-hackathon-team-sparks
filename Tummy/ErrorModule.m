//
//  ErrorModule.m
//  Tummy
//
//  Created by Priyanka on 12/01/16.
//  Copyright © 2016 Priyanka. All rights reserved.
//

#import "ErrorModule.h"

@implementation ErrorModule

+ (NSError *)errorWithErrorCode:(NSInteger)code
{
    switch (code) {
        default : case ERROR_UNKNOWN_ERROR :
            return [NSError errorWithDomain:NSLocalizedString(@"Unknown error",@"An unknown error code") code:code userInfo:nil];
    }
}

+(NSError *)errorWithServerErrorCode:(NSString *)string
{
    if(1)
    {
        return [NSError errorWithDomain:NSLocalizedString(@"Unknown error",@"An unknown error code") code:0 userInfo:nil];
    }
    else
    {
        return [NSError errorWithDomain:NSLocalizedString(@"Unknown error",@"An unknown error code") code:0 userInfo:nil];
    }
}
@end
