//
//  LatLong.h
//  Tummy
//
//  Created by Priyanka on 5/14/16.
//  Copyright © 2016 Priyanka. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LatLong : NSObject
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@end
